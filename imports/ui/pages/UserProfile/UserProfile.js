import React from 'react';
import { Grid, Image, Loader, Statistic, Label, Icon, Item } from 'semantic-ui-react'

// Taken from: https://stackoverflow.com/a/2901298/2367913
const numberWithCommas = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};

export const SocialLinks = props => (
    props.social_links.map(sl => {
        let icon;
        switch (sl.service_name) {
            case 'Instagram':
                icon = "instagram";
                break;
            case 'Facebook':
                icon = "facebook square";
                break;
            case 'LinkedIn':
                icon = "linkedin square";
                break;
            case 'Pinterest':
                icon = "pinterest square";
                break;
            case 'Tumblr':
                icon = "tumblr square";
                break;
            case 'Twitter':
                icon = "twitter square";
                break;
            case 'Vimeo':
                icon = "vimeo square";
                break;
            case "YouTube":
                icon = "youtube square";
                break;
            case "Flickr":
                icon = "flickr";
                break;
        }
        return (
            <a key={sl.social_id} href={sl.url}>
                <Icon size="large" name={icon}/>
            </a>
        )
    })
);

export const BasicInfo = props => (
    <div style={{padding: 10}}>
        {props.loading ? <Loader style={{margin: 150}} active inline="centered" /> : (
            <div>
                <Image
                    rounded centered
                    as='a' href={props.url}
                    height={276} src={props.images[276]}
                />
                <h1>{props.name}</h1>
                <div style={{marginBottom: 20}}><SocialLinks social_links={props.social_links} /></div>
                {props.occupation &&
                    <Label className="user-occupation">
                        <Icon name='building outline'/> {props.occupation}
                    </Label>
                }
                <Label className="user-location">
                    <Icon name='marker' /> {props.location}
                </Label>
                {props.website &&
                    <a className="user-website" href={props.website}>
                        <Label>
                            <Icon name='external'/> {props.website}
                        </Label>
                    </a>
                }
                <Statistic.Group size="mini">
                    <Statistic className="user-views">
                        <Statistic.Value>{numberWithCommas(props.stats.views)}</Statistic.Value>
                        <Statistic.Label>Views</Statistic.Label>
                    </Statistic>
                    <Statistic className="user-appreciations">
                        <Statistic.Value>{numberWithCommas(props.stats.appreciations)}</Statistic.Value>
                        <Statistic.Label>Appreciations</Statistic.Label>
                    </Statistic>
                    <Statistic className="user-followers">
                        <Statistic.Value>{numberWithCommas(props.stats.followers)}</Statistic.Value>
                        <Statistic.Label>Followers</Statistic.Label>
                    </Statistic>
                    <Statistic className="user-following">
                        <Statistic.Value>{numberWithCommas(props.stats.following)}</Statistic.Value>
                        <Statistic.Label>Following</Statistic.Label>
                    </Statistic>
                    <Statistic className="user-comments">
                        <Statistic.Value>{numberWithCommas(props.stats.comments)}</Statistic.Value>
                        <Statistic.Label>Comments</Statistic.Label>
                    </Statistic>
                </Statistic.Group>
            </div>
        )}
    </div>
);

export const Projects = props => (
    props.loading ? <Loader style={{margin: 150}} active inline="centered" /> : (
        <Item.Group link>
            {props.projects.map(p => (
                <Item href={p.url} key={p.id}>
                    <Item.Image size='small' src={p.covers[202]} />
                    <Item.Content>
                        <Item.Header className="project-name">{p.name}</Item.Header>
                        <Item.Meta className="project-fields">{p.fields.join(', ')}</Item.Meta>
                        <Item.Extra className="project-modified">Last Modified: {(new Date(p.modified_on * 1000)).toLocaleString()}</Item.Extra>
                        <Item.Description>
                            <Statistic.Group size="mini">
                                <Statistic className="project-views">
                                    <Statistic.Value>{numberWithCommas(p.stats.views)}</Statistic.Value>
                                    <Statistic.Label>Views</Statistic.Label>
                                </Statistic>
                                <Statistic className="project-appreciations">
                                    <Statistic.Value>{numberWithCommas(p.stats.appreciations)}</Statistic.Value>
                                    <Statistic.Label>Appreciations</Statistic.Label>
                                </Statistic>
                                <Statistic className="project-comments">
                                    <Statistic.Value>{numberWithCommas(p.stats.comments)}</Statistic.Value>
                                    <Statistic.Label>Comments</Statistic.Label>
                                </Statistic>
                            </Statistic.Group>
                        </Item.Description>
                    </Item.Content>
                </Item>
            ))}
        </Item.Group>
    )
);

export const WorkExperience = props => (
    props.loading ? <Loader style={{margin: 150}} active inline="centered" /> : (
        <Item.Group divided>
            {props.work_experience.map((w, i) => (
                <Item key={`work_experience_${i}`}>
                    <Item.Content>
                        <Item.Header className="work-organization">{w.organization}</Item.Header>
                        <Item.Meta className="work-location">{w.location}</Item.Meta>
                        <Item.Description className="work-position">{w.position}</Item.Description>
                    </Item.Content>
                </Item>
            ))}
        </Item.Group>
    )
);

export const Follows = props => (
    props.loading ? <Loader style={{margin: 150}} active inline="centered" /> : (
        <Label.Group>
            {props.follows.map(f => (
                <Label className="follow-label" style={{marginBottom: 8}} key={`${props.type}_${f.id}`} as='a' basic image href={f.url}>
                    <img className="follow-image" src={f.images[50]} />
                    {f.display_name || 'No Name'}
                </Label>
            ))}
        </Label.Group>
    )
);

export default class SearchUsers extends React.Component {
    state = {
        loadingBasicInfo: true,
        loadingProjects: true,
        loadingWorkExperience: true,
        basicInfo: {},
        projects: [],
        work_experience: [],
        followers: [],
        following: []
    };
    componentWillMount() {
        Meteor.call("getUserInfo", this.props.id, (err, res) => {
            //console.log(err, res);
            if (err) {
                this.setState({loadingBasicInfo: false});
            } else {
                const {data} = res;
                const {http_code, user} = data;
                if (http_code === 200) {
                    this.setState({loadingBasicInfo: false, basicInfo: {...user}});
                } else {
                    this.setState({loadingBasicInfo: false});
                }
            }
        });
        Meteor.call("getUserProjects", this.props.id, (err, res) => {
            //console.log(err, res);
            if (err) {
                this.setState({loadingProjects: false});
            } else {
                const { data } = res;
                const { http_code, projects } = data;
                if (http_code === 200) {
                    this.setState({loadingProjects: false, projects});
                } else {
                    this.setState({loadingProjects: false});
                }
            }
        });
        Meteor.call("getUserWorkExperience", this.props.id, (err, res) => {
            //console.log(err, res);
            if (err) {
                this.setState({loadingWorkExperience: false});
            } else {
                const { data } = res;
                const { http_code, work_experience } = data;
                if (http_code === 200) {
                    this.setState({loadingWorkExperience: false, work_experience});
                } else {
                    this.setState({loadingWorkExperience: false});
                }
            }
        });
        Meteor.call("getUserFollowers", this.props.id, (err, res) => {
            //console.log(err, res);
            if (err) {
                this.setState({loadingFollowers: false});
            } else {
                const { data } = res;
                const { http_code, followers } = data;
                if (http_code === 200) {
                    this.setState({loadingFollowers: false, followers});
                } else {
                    this.setState({loadingFollowers: false});
                }
            }
        });
        Meteor.call("getUserFollowing", this.props.id, (err, res) => {
            //console.log(err, res);
            if (err) {
                this.setState({loadingFollowing: false});
            } else {
                const { data } = res;
                const { http_code, following } = data;
                if (http_code === 200) {
                    this.setState({loadingFollowing: false, following});
                } else {
                    this.setState({loadingFollowing: false});
                }
            }
        });
    }
    render() {
        const {
            loadingBasicInfo,
            loadingProjects,
            loadingWorkExperience,
            loadingFollowers,
            loadingFollowing,
            basicInfo,
            projects,
            work_experience,
            followers,
            following
        } = this.state;
        const { display_name, images, location, occupation, stats, url, website, social_links } = basicInfo;
        return (
            <Grid stackable celled='internally'>
                <Grid.Row centered>
                    <BasicInfo
                        loading={loadingBasicInfo}
                        name={display_name}
                        images={images}
                        stats={stats}
                        location={location}
                        occupation={occupation}
                        url={url}
                        website={website}
                        social_links={social_links}
                    />
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column width={6}>
                        {(work_experience.length > 0 || loadingWorkExperience) && <h2>Work Experience</h2>}
                        <WorkExperience
                            loading={loadingWorkExperience}
                            work_experience={work_experience}
                        />
                        {(followers.length > 0 || loadingFollowers) && <h2>Followers</h2>}
                        <Follows
                            type="follower"
                            loading={loadingFollowers}
                            follows={followers}
                        />
                        {(following.length > 0 || loadingFollowing) && <h2>Following</h2>}
                        <Follows
                            type="following"
                            loading={loadingFollowing}
                            follows={following}
                        />
                    </Grid.Column>
                    <Grid.Column width={10} centered>
                        <h2>Projects</h2>
                        <Projects
                            loading={loadingProjects}
                            projects={projects}
                        />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}