import React from 'react';
import debounce from 'lodash.debounce';
import { Search } from 'semantic-ui-react'

export default class SearchUsers extends React.Component {
    state = {
        searchText: "",
        isLoading: false,
        results: []
    };

    _handleSearchChange = e => {
        //console.log('_handleSearchChange', e);
        const searchText = e.target.value;
        this.setState({searchText, isLoading: true});
        this._searchBehance(searchText);
    };

    _searchBehance = debounce(q => {
        //console.log('_searchBehance', q);
        Meteor.call('search', q, (err, res) => {
            console.log(err, res);
            const { data } = res;
            const { http_code, users } = data;
            let results = [];
            if (http_code === 200) {
                results = users.map(u => ({
                    title: u.first_name + ' ' + u.last_name,
                    image: u.images[100],
                    description: u.username,
                    id: u.id
                }))
            }
            this.setState({isLoading: false, results});
        });
    }, 300);

    _handleResultSelect = (e, data) => {
        //console.log('selected:', data);
        FlowRouter.go(`/user/${data.result.id}`);
    };

    render() {
        const { isLoading, searchText, results } = this.state;
        return (
            <div style={{width: '100vw', height: '100vh', display: 'flex', alignItems: 'center', flexDirection: 'column', padding: '10px'}}>
                <h1>Search for Users</h1>
                <Search
                    style={{width: '100%', display: 'flex', justifyContent: 'center'}}
                    fluid
                    loading={isLoading}
                    onResultSelect={this._handleResultSelect}
                    onSearchChange={this._handleSearchChange}
                    results={results}
                    value={searchText}
                />
            </div>
        )
    }
}