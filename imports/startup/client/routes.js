import { FlowRouter } from 'meteor/kadira:flow-router';
import { mount } from 'react-mounter';
// Import needed components
import SearchUsers from '/imports/ui/pages/SearchUsers/SearchUsers';
import UserProfile from '/imports/ui/pages/UserProfile/UserProfile';

// Set up all routes in the app
FlowRouter.route('/', {
    name: 'UserSearch',
    action() {
      mount(SearchUsers)
    },
});

FlowRouter.route('/user/:id', {
    name: 'UserProfile',
    action(params) {
        mount(UserProfile, { id: params.id })
    },
});
