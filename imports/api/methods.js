import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';

Meteor.methods({
    "search": function (q) {
        check(q, String);
        const result = HTTP.get(
            `http://behance.net/v2/users`,
            {
                params: {
                    client_id : Meteor.settings.behance,
                    q
                }
            }
        );
        return result;
    },
    "getUserInfo": function (id) {
        check(id, Match.OneOf(Number, String));
        const result = HTTP.get(
            `http://behance.net/v2/users/${id}`,
            {
                params: {
                    client_id : Meteor.settings.behance
                }
            }
        );
        return result;
    },
    "getUserProjects": function (id) {
        check(id, Match.OneOf(Number, String));
        const result = HTTP.get(
            `http://behance.net/v2/users/${id}/projects`,
            {
                params: {
                    client_id : Meteor.settings.behance
                }
            }
        );
        return result;
    },
    "getUserWorkExperience": function (id) {
        check(id, Match.OneOf(Number, String));
        try {
            const result = HTTP.get(
                `http://behance.net/v2/users/${id}/work_experience`,
                {
                    params: {
                        client_id: Meteor.settings.behance
                    }
                }
            );
            return result;
        } catch (e) {
            console.log('error caught', e);
            throw new Meteor.Error(404, "No work experience found");
        }
    },
    "getUserFollowers": function (id) {
        check(id, Match.OneOf(Number, String));
        const result = HTTP.get(
            `http://behance.net/v2/users/${id}/followers`,
            {
                params: {
                    client_id : Meteor.settings.behance
                }
            }
        );
        return result;
    },
    "getUserFollowing": function (id) {
        check(id, Match.OneOf(Number, String));
        const result = HTTP.get(
            `http://behance.net/v2/users/${id}/following`,
            {
                params: {
                    client_id : Meteor.settings.behance
                }
            }
        );
        return result;
    },
});
